# ChatGPT-uniapp openai 兑换码，分享，会员APP，H5，小程序，微信公众号、微信支付、首页多模板切换

## EasyChatGPT 

uni-app 前端 会员次数充值（公众号支付与小程序支付），会员兑换码，分享，一键部署，独立后台，卡密兑换，AI对话

后台 elementUI实现的简约大气chatgpt后台管理系统🚀🚀🚀

实现用户、邀请、聊天、兑换码（对接第三方发卡网，导出）、帮助内容、系统级配置（邀请功能开关、提示公告等）、接口内容安全检查、角色设定、限制词、openai api key 管理、key轮询等安全功能、支付购买次数、套餐设置、首页模板等

服务端 完整版（298/套，随功能更新调价，老用户永久免费更新）前后端全开源附带RBAC权限控制、定时任务、日志、短信验证码、oss托管等

服务端使用php7.4，基于webman 1.4 框架，实现随机调用openai apikey，使用GPT3.5模型实现前端流式输出（参考小程序正式版）

如需key可加群联系作者获取体验

**注意：开源项目为uniapp用户的和管理后台前端的mock模板，所有数据均为mock，非真实接口数据。服务端是不公开开源的，需要联系作者获取。** 

[服务端部署教程](https://juejin.cn/post/7226689042406391864)

[最全面视频部署教程，完整版可免费技术指导](https://www.acfun.cn/v/ac41237548)

[最全面前端视频部署教程，完整版可免费技术指导](https://www.acfun.cn/v/ac41243139)

## h5用户端demo

[https://newgpt.nauzone.cn](https://newgpt.nauzone.cn)

[uniapp插件市场地址](https://ext.dcloud.net.cn/plugin?id=11603)

| h5端 | 微信小程序 |
| :----: | :----: |
| ![h5端](https://qnoss.bazhuayu.nauzone.cn/gpt/images/h5url.png) 微信扫码打开即可登录h5端  | ![微信小程序](https://qnoss.bazhuayu.nauzone.cn/gpt/images/aicsw.jpg) |

## 后台demo


[后台开源仓库](https://gitee.com/shoujing1001/chatgpt-admin)

| 系统截图 | **服务端完整版微信群获取** |
| :----: | :----: |
| ![系统配置](https://qnoss.bazhuayu.nauzone.cn/gpt/images/config.png)  | ![交流群](https://qnoss.bazhuayu.nauzone.cn/gpt/images/group0429.jpg) |



## Project setup

HbuilderX编辑器导入项目

```
npm install
```

运行

## 运行截图

### 移动用户端截图

| 首页模板（直接提问） | 首页（主模型） | 首页（AI创作）|
| :----: | :----: | :----: |
| ![首页模板（直接提问](https://qnoss.bazhuayu.nauzone.cn/gpt/images/index_template_qa.jpg)  |   ![首页（主模型）](https://qnoss.bazhuayu.nauzone.cn/gpt/images/home.jpg)  |  ![首页（AI创作）](https://qnoss.bazhuayu.nauzone.cn/gpt/images/index_template_write.jpg)  |

| 首页（直接聊天） | 开通权益 | 切换模型 |
| :----: | :----: | :----: |
| ![首页（直接聊天）](https://qnoss.bazhuayu.nauzone.cn/gpt/images/index_template_chat_2.jpg)  | ![开通权益](https://qnoss.bazhuayu.nauzone.cn/gpt/images/vip.jpg)  | ![切换模型](https://qnoss.bazhuayu.nauzone.cn/gpt/images/assistant.jpg) |


| 用户中心 | 兑换码使用 | 聊天 |
| :----: | :----: | :----: |
| ![用户中心](https://qnoss.bazhuayu.nauzone.cn/gpt/images/center.jpg)  | ![兑换次数](https://qnoss.bazhuayu.nauzone.cn/gpt/images/redePop.jpg) |![聊天](https://qnoss.bazhuayu.nauzone.cn/gpt/images/chat.jpg)|

| 注册 | 登录 | 公众号登录 |
| :----: | :----: | :----: |
| ![注册](https://qnoss.bazhuayu.nauzone.cn/gpt/images/reg.jpg)  | ![登录](https://qnoss.bazhuayu.nauzone.cn/gpt/images/loginh5.jpg) | ![公众号登录](https://qnoss.bazhuayu.nauzone.cn/gpt/images/gzhdl.jpg)

### 后台管理截图

角色配置
![角色配置](https://qnoss.bazhuayu.nauzone.cn/gpt/images/assistant.png)

openAi ApiKey管理
![key管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/openkey.png)

订单管理
![订单管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/order.png)

权益套餐管理
![权益套餐管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/comb.png)

登录
![登录](https://qnoss.bazhuayu.nauzone.cn/gpt/images/login.png)

用户管理
![用户管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/member.png)

聊天管理
![聊天管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/chat.png)

消息管理
![消息管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/message.png)

兑换码管理
![兑换码管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/rede.png)

内容管理
![内容管理](https://qnoss.bazhuayu.nauzone.cn/gpt/images/content.png)

系统配置
![系统配置](https://qnoss.bazhuayu.nauzone.cn/gpt/images/config.png)

角色权限、定时任务等...（完整版，请联系作者）
![角色权限](https://qnoss.bazhuayu.nauzone.cn/gpt/images/role.png)
